function createBoard() {
    // TODO: get width and height from input fields
    let board = [];
    for (let i = 0; i < 10; i++) {
        board.push([]);
        for (let j = 0; j < 10; j++) {
            /** 0 represents an empty cell
            * numbers 1-8 represent the number of mines adjacent to the cell
            * 'b' represents a mine
            * The board is initialized with no mines
            */
            board[i].push(0);
        }
    }
    gameBoard = board;
}

function setUpMines(difficulty) {
    /**
     * This function sets up the mines on the board.
     * difficulty is a percentage of the board that will be mines.
     * For example, if difficulty is 0.1, then 10% of the board will be mines.
     *
     * The mines are randomly placed on the board.
     * The number of mines adjacent to each cell is calculated and stored in the board.
     */
    // TODO: implement this function
    gameBoard[0][0] = 'b';
    gameBoard[0][1] = '1';
    gameBoard[1][0] = '1';
    gameBoard[1][1] = '1';
}

function renderBoard() {
    // This function renders the board to the DOM
    const boardElement = document.querySelector('#board');
    boardElement.innerHTML = '';
    const rootTable = document.createElement('table');
    rootTable.setAttribute('cellspacing', '0');
    for (let i = 0; i < gameBoard.length; i++) {
        const row = document.createElement('tr');
        for (let j = 0; j < gameBoard[i].length; j++) {
            const cell = document.createElement('td');
            cell.setAttribute('id', `cell-${i}-${j}`);
            cell.classList.add('hidden');
            cell.addEventListener('click', () => {
                cellHandler(j, i);
            })
            row.appendChild(cell);
        }
        rootTable.appendChild(row);
    }
    boardElement.appendChild(rootTable);
}

function cellHandler(x, y) {
    /**
     * This functions acts as a router for the different actions that can be taken on a cell.
     * Already revealed cell: do nothing
     * Left clicks:
     * - If the cell is hidden, call the revealCell function.
     * - If the cell is a mine, end the game.
     * Right clicks:
     * - Flag the cell if not already flagged
     * - Unflag the cell if already flagged
     */
    console.log(`Clicked on cell ${x}, ${y}`);
    console.log('Item in cell: ', gameBoard[y][x]);
    // TODO: implement this function
}

function revealCell(x, y) {
    /**
     * reveals the cell at x, y
     * if the cell is empty, reveal all adjacent cells
     */
    const cell = document.querySelector(`#cell-${y}-${x}`);
    // TODO: implement this function
}


function getNumberOfMines() {
    return gameBoard.flat().filter(item => item === 'b').length;
}

function getNumberOfHiddenCells() {
    return document.querySelectorAll('.hidden').length;
}


// You can refactor these function into one "toggleFlag" function if you want
function flagCell(x, y) {
    const cell = document.querySelector(`#cell-${y}-${x}`);
    cell.classList.add('flagged');
}

function unFlagCell(x, y) {
    const cell = document.querySelector(`#cell-${y}-${x}`);
    cell.classList.remove('flagged');
}


let gameBoard = null;
function newGame() {
    createBoard();
    setUpMines(0.2);

    renderBoard();
}