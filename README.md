# Minesweeper JS

In this exercise, you will be building a Minesweeper game in JavaScript. The game will be played in the browser, and will be styled using CSS.

**The `style.css` file already contains some basic styling for the game.**

## Part 1: Setup

The game board will be a contained in a 2D array, and rendered to the browser using simple table elements.

1. The createBoard function creates a 10 x 10 table. Instead, read the desired board size from the inputs fields.
2. Once the board is created, the mines need to be placed. Implement the placeMines function to randomly place mines on the board.
3. While the mines are being placed, the adjacent cells need to be updated to reflect the number of mines in the surrounding cells.

![place](assets/img.png)

## Part 2: Cell management

1. Implement first logic on the click event of the cells:
   1. Distinguish between a left click and a right click (this will be useful later).
   2. if the cell is a mine and not revealed, the game is over.
   3. if the cell is not a mine and not revealed, call the revealCell function.
   4. if the cell is revealed, do nothing.
2. Implement the revealCell function to reveal the cell that was clicked. If the cell is empty, the surrounding cells are to be revealed as well. This implies that all the connected cells that are empty need to be revealed at once, plus all the non-empty cells that are adjacent to the empty cells (numbers).
3. A revealed cell containing a number should display its value.

Notes:
- Be careful about infinite recursion. Be sure not to call the revealCell function on a cell that has already been revealed.

![reveal](assets/reveal.png)

## Part 3: Game management

1. On right click, the cell should be flagged. If the cell is already flagged, it should be unflagged.
2. If the number of remaining mines is equal to the number of remaining hidden cells, the game is won.

## Part 4: Repeatability

As a bonus, implement the game logic:
1. When the game is over, the user should be notified.
2. The user should be able to restart the game at any time.
3. Keep track of the time since the game started, and the number of victories/defeats.

## Part 5: Why not.

- Add some style!
- Add a difficulty level (easy, medium, hard).

